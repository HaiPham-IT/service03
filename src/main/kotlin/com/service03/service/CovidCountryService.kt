package com.service03.service

import com.service03.model.CovidCountry
import org.springframework.data.domain.Page

interface CovidCountryService {
    fun getAll(pageIndex: Int, pageSize: Int): Page<CovidCountry>
    fun search(searchstr: String, pageIndex: Int, pageSize: Int): Page<CovidCountry>
}