package com.service03

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class Service03Application

fun main(args: Array<String>) {
	runApplication<Service03Application>(*args)
}
