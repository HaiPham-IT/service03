package com.service03.service.impl

import com.service03.model.CovidCountry
import com.service03.repository.CovidCountryRepository
import com.service03.service.CovidCountryService
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.stereotype.Service

@Service
class CovidCountryServiceImpl(
    private var covidCountryRepository: CovidCountryRepository
): CovidCountryService {
    override fun getAll(pageIndex: Int, pageSize: Int): Page<CovidCountry>
    = covidCountryRepository.findAll(PageRequest.of(if(pageIndex > 0) pageIndex -1 else pageIndex, pageSize))

    override fun search(searchstr: String, pageIndex: Int, pageSize: Int): Page<CovidCountry>
    = when(searchstr){
        "" -> getAll(pageIndex, pageSize)
        else -> covidCountryRepository.search(searchstr, PageRequest.of(if(pageIndex > 0) pageIndex -1 else pageIndex, pageSize))
    }
}