package com.service03.service.impl

import com.service03.model.CovidCountry
import com.service03.repository.CovidCountryRepository
import com.service03.service.CovidCountryService
import io.mockk.*
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.data.domain.PageImpl
import org.springframework.data.domain.PageRequest

internal class CovidCountryServiceImplTest {

    private val covidCountryRepository = mockk<CovidCountryRepository>(relaxed = true)
    private val covidCountryService: CovidCountryService = CovidCountryServiceImpl(covidCountryRepository)

    @BeforeEach
    fun setup() {
        mockkStatic(PageRequest::class)
    }

    @AfterEach
    fun teardown(){
        unmockkAll()
    }

    private val c1 = CovidCountry("1", "Vietnam", "Asia", 10000, 1000, 9000, "https://disease.sh/")
    private val c2 = CovidCountry("2", "Wallis and Futuna", "Australia-Oceania", 761, 7, 438, "https://api.coronatracker.com/")
    private val c3 = CovidCountry("3", "Western Sahara", "Africa", 10000, 1000, 9000, "https://disease.sh/")
    private val c4 = CovidCountry("2", "Yemen", "Asia", 761, 7, 438, "https://api.coronatracker.com/")

    @Test
    fun `get all data with pageIndex and pageSize`() {
        val objRecords = mutableListOf(c1, c2, c3, c4)
        val covidPage = PageImpl(objRecords, PageRequest.of(0,2), 0)

        every { covidCountryRepository.findAll(PageRequest.of(any(),any())) } returns covidPage

        val records = covidCountryService.getAll(1,2)

        verify { covidCountryRepository.findAll(PageRequest.of(any(),any())) }
        Assertions.assertNotNull(records.content)
        Assertions.assertEquals(2, records.size )//pageSize
        Assertions.assertEquals(1, records.number+1)//pageIndex
        Assertions.assertEquals(2, records.totalPages)//totalPage
    }

    @Test
    fun `search data with empty string`() {
        val service: CovidCountryServiceImpl = spyk(CovidCountryServiceImpl(covidCountryRepository))
        val objRecords = mutableListOf(c1, c2, c3, c4)
        val covidPage = PageImpl(objRecords, PageRequest.of(0,2), 0)

        every { covidCountryRepository.findAll(allAny<PageRequest>()) } returns covidPage

        val records = service.search("", 1,2)

        verify { service.getAll(allAny(), allAny()) }
        Assertions.assertNotNull(records.content)
        Assertions.assertEquals(2, records.size )//pageSize
        Assertions.assertEquals(1, records.number+1)//pageIndex
        Assertions.assertEquals(2, records.totalPages)//totalPage
    }

    @Test
    fun `search data with any string`(){
        val objRecords = mutableListOf(c1, c4)
        val covidPageMock = PageImpl(objRecords, PageRequest.of(0,2), 0)

        every { covidCountryRepository.search(any(), PageRequest.of(any(), any())) } returns covidPageMock

        val records = covidCountryService.search("Asia",1,2)

        verify { covidCountryRepository.search(any(), PageRequest.of(any(), any())) }
        Assertions.assertNotNull(records.content)
        Assertions.assertEquals(2, records.size )//pageSize
        Assertions.assertEquals(1, records.number+1)//pageIndex
        Assertions.assertEquals(1, records.totalPages)//totalPage
    }
}