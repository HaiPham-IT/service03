package com.service03.controller

import com.service03.dto.PageList
import com.service03.service.CovidCountryService
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@CrossOrigin
@RestController
@RequestMapping("/covidcountry")
class CovidCountryController(private var covidCountryService: CovidCountryService) {

    @GetMapping("/")
    fun getAll(
        @RequestParam(defaultValue = "0") pageIndex: Int,
        @RequestParam(defaultValue = "100") pageSize: Int): ResponseEntity<Any>{
        val records = covidCountryService.getAll(pageIndex, pageSize)
        return ResponseEntity(PageList(records.number+1, records.totalPages, records.size, records.content.size, records.totalElements, records.content), HttpStatus.OK)
    }
    @GetMapping("/search")
    fun search(
        @RequestParam(defaultValue = "") searchstr: String,
        @RequestParam(defaultValue = "0") pageIndex: Int,
        @RequestParam(defaultValue = "100") pageSize: Int): ResponseEntity<Any>{
        val records = covidCountryService.search(searchstr, pageIndex, pageSize)
        return ResponseEntity(PageList(records.number+1, records.totalPages, records.size, records.content.size, records.totalElements, records.content), HttpStatus.OK)
    }
}