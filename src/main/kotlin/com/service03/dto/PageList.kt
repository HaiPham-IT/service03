package com.service03.dto

data class PageList<T>(
    var pageIndex: Int?,
    var totalPage: Int?,
    var pageSize: Int?,
    var curSize: Int?,
    var total: Long?,
    var list: List<T>?
) {
}