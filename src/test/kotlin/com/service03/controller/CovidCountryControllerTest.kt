package com.service03.controller

import com.service03.model.CovidCountry
import com.service03.service.CovidCountryService
import io.mockk.every
import io.mockk.mockk
import io.mockk.unmockkAll
import io.mockk.verify
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Test

import org.junit.jupiter.api.BeforeEach
import org.springframework.data.domain.PageImpl
import org.springframework.data.domain.PageRequest
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import org.springframework.test.web.servlet.setup.MockMvcBuilders

internal class CovidCountryControllerTest {

    private val covidCountryService = mockk<CovidCountryService>()
    private val covidCountryController = CovidCountryController(covidCountryService)
    private val mockMvc = MockMvcBuilders.standaloneSetup(covidCountryController).build()

    private val c1 = CovidCountry("1", "Vietnam", "Asia", 10000, 1000, 9000, "https://disease.sh/")
    private val c2 = CovidCountry("2", "Wallis and Futuna", "Australia-Oceania", 761, 7, 438, "https://api.coronatracker.com/")
    private val c3 = CovidCountry("3", "Western Sahara", "Africa", 10000, 1000, 9000, "https://disease.sh/")
    private val c4 = CovidCountry("2", "Yemen", "Asia", 761, 7, 438, "https://api.coronatracker.com/")

    @BeforeEach
    fun setup(){

    }

    @AfterEach
    fun teardown(){
        unmockkAll()
    }

    @Test
    fun getAll() {
        val pageRecords = PageImpl(listOf(c1, c2, c3, c4), PageRequest.of(0,2), 0)

        every { covidCountryService.getAll(allAny(), allAny()) } returns pageRecords

        mockMvc.perform(MockMvcRequestBuilders.get("/covidcountry/").accept(MediaType.APPLICATION_JSON))
            .andExpect(MockMvcResultMatchers.status().isOk)
        verify { covidCountryService.getAll(allAny(), allAny()) }
    }

    @Test
    fun search() {
        val pageRecords = PageImpl(listOf(c1, c2, c3, c4), PageRequest.of(0,2), 0)

        every { covidCountryService.search(allAny(), allAny(), allAny()) } returns pageRecords

        mockMvc.perform(MockMvcRequestBuilders.get("/covidcountry/search/").accept(MediaType.APPLICATION_JSON))
            .andExpect(MockMvcResultMatchers.status().isOk)
        verify { covidCountryService.search(allAny(), allAny(), allAny()) }
    }
}