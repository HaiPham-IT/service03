package com.service03.repository

import com.service03.model.CovidCountry
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.elasticsearch.annotations.Query
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository
import org.springframework.stereotype.Repository

@Repository
interface CovidCountryRepository: ElasticsearchRepository<CovidCountry, String> {
    @Query("{ \"bool\" : { \"should\" : [ { \"query_string\" : { \"query\" : \"?0\", \"fields\" : [ \"country\", \"continent\" ] } } ] } }")
    fun search(searchstr: String, pageable: Pageable): Page<CovidCountry>
}